﻿using UEW;

namespace TestWorld
{
    public class TestCreature : Creature
    {
        public int a = 0;
        public TestCreature()
        {
            
        }

        protected override void Processing()
        {
            ++a;
        }
    }
}
