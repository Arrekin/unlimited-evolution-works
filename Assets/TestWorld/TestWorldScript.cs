﻿using UEW.World.Date;
using UnityEngine;

namespace TestWorld
{
    public class TestWorldScript : MonoBehaviour
    {
        private TestCreature c;
        // Use this for initialization
        void Start ()
        {
	        c = new TestCreature();
            c.Animate();
        }
	
        // Update is called once per frame
        void Update ()
        {
            Debug.Log(c.a);
        }

        void OnDestroy()
        {
            c.Kill();
        }
    }
}
