﻿using System;
using UEW.Bodies;
using UEW.Interfaces;
using UEW.Utils;
using UEW.World;
using UEW.World.Date;
using UnityEngine;

namespace UEW.BodyComponents
{
    /// <summary>
    /// Basic energy storage class
    /// Energy Consumption:
    ///     - Base: 0.01 (per world step)
    ///     - Improvements: 
    ///         - 0.001 per every CapacitPoint that exceeding 1000
    ///     - Usage: No Cost
    /// Additional Info:
    ///     - Request energy checks whether requesting componenet belongs to the same body
    ///     - Generates only tickets valid for one world step
    /// DateType: LinearTime
    /// </summary>
    public class BasicEnergyStorage : IBodyComponent, IEnergyStorage
    {
        /// <summary>
        /// Basic component constructor.
        /// </summary>
        /// <param name="body"> Main body conteiner.</param>
        public BasicEnergyStorage(Body body)
        {
            Body = body;
        }
        #region IEnergyStorage
        public virtual float Energy { get; protected set; }
        public virtual float EnergyCapacity { get; set; }

        /// <summary>
        /// Sends request for new energy ticket. After this method finishes componenet should have receive new energy ticket.
        /// </summary>
        /// <param name="requestingComponent"> Componenet that requests energy.</param>
        /// <param name="energyAmount"> amount of energy to request.</param>
        public virtual void EnergyRequest(IBodyComponent requestingComponent, float energyAmount)
        {
            // Anti-crossbody request validation
            if (requestingComponent.Body != Body) return;
            
            // Anti-reversed energy flow validation(0 is valid, ticket will be empty but generated correctly)
            if (energyAmount < 0) return;

            var amount = Math.Min(Energy, energyAmount);
            Energy -= amount;
            var ticket = new EnergyTicket(amount, WorldDate.CreateDate<LinearTime>((LinearTime)Body.World.CurrentDate,1f));
            requestingComponent.ReceiveEnergyTicket(ticket);
        }

        /// <summary>
        /// Adds energy to the storage.
        /// </summary>
        /// <param name="senderComponenet"> Component that passing energy.</param>
        /// <param name="energyAmount"> Amount of energy to store.</param>
        public virtual void StoreEnergy(IBodyComponent senderComponenet, float energyAmount)
        {
            Energy = Mathf.Min(EnergyCapacity, Energy + energyAmount);
        }
        #endregion

        #region IBodyComponent

        public Body Body { get; private set; }
        public virtual float BaseEnergyConsumption { get { return 0.01f; } }
        public virtual float HealthPoints { get; protected set; }

        /// <summary>
        /// This component energy consumption is 0.001 EP for every energy storage point above 1000.
        /// </summary>
        public virtual float EnergyConsumption()
        {
            return BaseEnergyConsumption + Math.Max(0, EnergyCapacity - 1000)*0.001f; // Every additional Energy storage point consume 0.01 energy
        }

        /// <summary>
        /// Energy Ticket for componenet send by IEnergyStorage component.
        /// </summary>
        /// <param name="energyTicket"> Energy Ticket with fresh energy.</param>
        public virtual void ReceiveEnergyTicket(EnergyTicket energyTicket)
        {
            currentEnergyTicket = energyTicket;
        }

        /// <summary>
        /// Sends request for energy ticket to linked energy storage component
        /// Requested amount is equal to EnergyConsumption() as this component does not need additional energy to perform work
        /// </summary>
        public virtual void Feed()
        {
            var energyForLiving = EnergyConsumption();
            var energyStorage = Body.GetEnergyStorage(this);
            if (energyStorage == null) return;

            energyStorage.EnergyRequest(this, energyForLiving);

            //use ticket; when there is no ticket or no enaugh energy component starts losing HealtPoints
            if (currentEnergyTicket != null && currentEnergyTicket.IsValid(Body.World))
            {
                var gatheredEnergy = currentEnergyTicket.GrantEnergy(energyForLiving, Body.World);
                if (!gatheredEnergy.Equals(energyForLiving))
                {
                    HealthPoints -= (energyForLiving - gatheredEnergy)*100;
                }
            }
            else
            {
                HealthPoints -= 2;
            }
        }

        #endregion


        #region Private
        /// <summary>
        /// This componenet like every other needs energy to work. This is its personal energy ticket
        /// </summary>
        protected EnergyTicket currentEnergyTicket;

        #endregion
    }
}
