﻿using System;
using System.Threading;
using UEW.Interfaces;

namespace UEW.Utils
{
    public class BodyComponentConnector<TBodyComponent> where TBodyComponent : IBodyComponent
    {
        /// <summary>
        /// Returns linked body component
        /// </summary>
        public TBodyComponent Component { get; private set; }

        /// <summary>
        /// Internal wait handler dedicated to related body componenet
        /// </summary>
        private readonly EventWaitHandle internalWaitHandler;

        /// <summary>
        /// External wait handle, allows creture to have its own handler that is set when any body component refreshes
        /// </summary>
        private EventWaitHandle externalWaitHandle;

        public BodyComponentConnector(TBodyComponent bodyComponent)
        {
            if (bodyComponent == null)
                throw new NullReferenceException("BodyComponentConnector creation without valid BodyComponent object");
            Component = bodyComponent;

            internalWaitHandler = new EventWaitHandle(false,EventResetMode.AutoReset);
        }

        /// <summary>
        /// Pausing thread until related body component signals new data
        /// </summary>
        public void WaitForNewDataSignal()
        {
            internalWaitHandler.WaitOne();
        }

        /// <summary>
        /// Sets new external wait handle - its usefull when there is more body component and we want to get notified when any of them get fresh data
        /// </summary>
        /// <param name="waitHandle"> wait handle that will be signaled on fresh data event</param>
        public void SetExternalWaitHandle(EventWaitHandle waitHandle)
        {
            externalWaitHandle = waitHandle;
        }

        /// <summary>
        /// Body componenets use this method to signal aviability of fresh data
        /// </summary>
        public void FreshDataSignal()
        {
            internalWaitHandler.Set();
            if (externalWaitHandle != null)
            {
                externalWaitHandle.Set();
            }
        }
    }
}
