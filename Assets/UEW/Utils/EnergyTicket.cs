﻿using UEW.Interfaces;
using UEW.World.Date;
using UnityEngine;

namespace UEW.Utils
{
    public class EnergyTicket
    {
        /// <summary>
        /// Energy Ticket class constructor.
        /// </summary>
        /// <param name="energyAmount"> Initial value of energy stored inside ticket.</param>
        /// <param name="expirationDate"> The Date after the ticket cannot be used anymore.</param>
        public EnergyTicket(float energyAmount, WorldDate expirationDate)
        {
            Energy = energyAmount;
            ExpirationDate = expirationDate;
        }
        
        /// <summary>
        /// Currnet amount of stored energy.
        /// </summary>
        public float Energy { get; private set; }

        /// <summary>
        /// Returns ticket's expiration date
        /// </summary>
        public WorldDate ExpirationDate { get; private set; }

        /// <summary>
        /// Returns whether the date is valid in relation to given world
        /// </summary>
        /// <param name="world"> To world the date should be compared</param>
        public bool IsValid(IWorld world)
        {
            return world.CurrentDate <= ExpirationDate;
        }

        /// <summary>
        /// Gather energy from ticket and returns it to requesting component
        /// </summary>
        /// <param name="amount"> Amount of requesting energy</param>
        /// <param name="world"> Related world</param>
        /// <returns></returns>
        public float GrantEnergy(float amount, IWorld world)
        {
            return IsValid(world) ? Mathf.Min(Energy, amount) : 0;
        }
    }
}