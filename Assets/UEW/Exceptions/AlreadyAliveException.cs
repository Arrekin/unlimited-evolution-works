﻿using System;

namespace UEW.Exceptions
{
    public class AlreadyAliveException : Exception
    {
        public AlreadyAliveException()
        {
        }

        public AlreadyAliveException(string message)
            : base(message)
        {
        }

        public AlreadyAliveException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
