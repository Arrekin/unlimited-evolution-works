﻿using System.Collections.Generic;
using UEW.Interfaces;
using UnityEngine;

namespace UEW.Bodies
{
    /// <summary>
    /// Base abstract class reponsible for creature world body as whole.
    /// </summary>
    public abstract class Body : MonoBehaviour
    {
        /// <summary>
        /// Reference to world in which this body exists
        /// </summary>
        public IWorld World { get; set; }

        /// <summary>
        /// List of all componenets building this body
        /// </summary>
        protected virtual List<IBodyComponent> BodyComponents { get; private set; }

        #region MonoBehaviour
        /// <summary>
        /// MonoBehaviour Start() method
        /// </summary>
        public virtual void Start()
        {
            BodyComponents = new List<IBodyComponent>();
        }
        #endregion

        /// <summary>
        /// Adds body component to the body
        /// </summary>
        /// <param name="componentToAttach"> Component to attach</param>
        public virtual void AttachComponent(IBodyComponent componentToAttach)
        {
            BodyComponents.Add(componentToAttach);
        }

        /// <summary>
        /// Returns energy storage component linked with requesting body component
        /// </summary>
        /// <param name="requestingComponent"></param>
        public abstract IEnergyStorage GetEnergyStorage(IBodyComponent requestingComponent);
    }
}
