﻿using UEW.Interfaces;

namespace UEW.Bodies
{
    /// <summary>
    /// Basic body class
    /// Details:
    ///     - Only 1 attached Energy storage component will work
    /// </summary>
    public class BasicBody : Body
    {
        /// <summary>
        /// Current component working as energy storage for the body
        /// </summary>
        private IEnergyStorage bodyEnergyStorage;

        /// <summary>
        /// Returns the last one registered energy storage body component
        /// </summary>
        /// <param name="requestingComponent"> Body Component that requesting its linked energy storage(int this case each component has the same energy storage component.</param>
        public override IEnergyStorage GetEnergyStorage(IBodyComponent requestingComponent)
        {
            return bodyEnergyStorage;
        }

        /// <summary>
        /// Attach component to body.
        /// If it is Energy Storage component set it as body current energy storage component.
        /// </summary>
        /// <param name="componentToAttach"> Component to attach.</param>
        public override void AttachComponent(IBodyComponent componentToAttach)
        {
            IEnergyStorage es = (IEnergyStorage) componentToAttach;
            if (es != null)
            {
                bodyEnergyStorage = es;
            }
            base.AttachComponent(componentToAttach);
        }
    }
}
