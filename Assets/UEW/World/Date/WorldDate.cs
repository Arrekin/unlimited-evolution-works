﻿using System;
using System.Reflection;

namespace UEW.World.Date
{
    /// <summary>
    /// Base class for storing world date info
    /// </summary>
    public abstract class WorldDate
    {
        /// <summary>
        /// Factory for world dates, pass any other date to get it successor
        /// </summary>
        /// <typeparam name="TDateType"> WorldDate class type</typeparam>
        /// <param name="previousDate"> Previous date</param>
        /// <param name="timeShift"> Shift in relation to previous date.</param>
        public static TDateType CreateDate<TDateType>(TDateType previousDate, float timeShift) where TDateType: WorldDate
        {
            return (TDateType)typeof(TDateType).GetConstructor(
                BindingFlags.NonPublic | BindingFlags.CreateInstance | BindingFlags.Instance,
                null,
                new[] { typeof(TDateType) },
                null
                ).Invoke(new object[] { previousDate, timeShift });
        }
        /// <summary>
        /// Factory for world dates, creates 0-day date
        /// </summary>
        /// <typeparam name="TDateType"> WorldDate class type</typeparam>
        public static TDateType CreateDate<TDateType>() where TDateType : WorldDate
        {
            return (TDateType)Activator.CreateInstance(typeof(TDateType), true);
        }

        public static bool operator <(WorldDate d1, WorldDate d2)
        {
            return d1 != null && d1.Before(d2);
        }

        public static bool operator <=(WorldDate d1, WorldDate d2)
        {
            return d1 != null && (d1.Before(d2) || d1.Equals(d2));
        }

        public static bool operator >(WorldDate d1, WorldDate d2)
        {
            return d1 != null && d1.After(d2);
        }

        public static bool operator >=(WorldDate d1, WorldDate d2)
        {
            return d1 != null && (d1.After(d2) || d1.Equals(d2));
        }

        public static bool operator ==(WorldDate d1, WorldDate d2)
        {
            return d1 != null && d1.Equals(d2);
        }

        public static bool operator !=(WorldDate d1, WorldDate d2)
        {
            return d1 != null && !d1.Equals(d2);
        }

        /// <summary>
        /// Checks whether the other date is equal to this one
        /// </summary>
        /// <param name="other"> Date to compare</param>
        public abstract bool Equals(WorldDate other);

        /// <summary>
        /// Checks whether this date is before the other one
        /// </summary>
        /// <param name="other"> Date to compare</param>
        public abstract bool Before(WorldDate other);

        /// <summary>
        /// Checks whether this date is after the other one
        /// </summary>
        /// <param name="other"> Date to compare</param>
        public abstract bool After(WorldDate other);

        protected WorldDate(WorldDate previousDate, float timeShift)
        {
        }

        protected WorldDate()
        {
        }
    }
}
