﻿namespace UEW.World.Date
{
    /// <summary>
    /// Linear time 1 full world loop = 1 time unit
    /// </summary>
    public class LinearTime : WorldDate
    {
        /// <summary>
        /// Date time value
        /// </summary>
        public long Time { get; protected set; }
        public override bool Equals(WorldDate other)
        {
            var o = (LinearTime)other;
            return o != null && Time == o.Time;
        }

        public override bool Before(WorldDate other)
        {
            var o = (LinearTime)other;
            return o != null && Time < o.Time;
        }

        public override bool After(WorldDate other)
        {
            var o = (LinearTime)other;
            return o != null && Time > o.Time;
        }

        /// <summary>
        /// Dates can be created only from WorldDate class. LinearTime just adds +1 to date from it was generated
        /// </summary>
        /// <param name="previousDate"> Base Date</param>
        /// <param name="timeShift"> Time shift - integer; number of steps</param>
        protected LinearTime(LinearTime previousDate, float timeShift) : base(previousDate, timeShift)
        {
            Time = previousDate.Time + (int)timeShift;
        }
        /// <summary>
        /// Dates can be created only from WorldDate class. No parameter constructor creates 0-day date.
        /// </summary>
        protected LinearTime()
        {
            Time = 0;
        }
    }
}
