﻿using System;
using UEW.Bodies;
using UnityEngine;

namespace UEW.World
{
    /// <summary>
    /// Class responsible for connecting creature logic with its world body
    /// </summary>
    public class Avatar : MonoBehaviour
    {
        /// <summary>
        /// Parent of every body componenets in given creature
        /// </summary>
        private Body body;

        public Avatar(Body body)
        {
            if( body == null)
                throw new NullReferenceException("Creating AVATAR instance without associated BODY object!");
            this.body = body;
        }


    }
}
