﻿namespace UEW.Interfaces
{
    public interface IEnergyStorage
    {
        /// <summary>
        /// Current amount of stored energy
        /// </summary>
        float Energy { get; }

        /// <summary>
        /// Maximal energy storage amount
        /// </summary>
        float EnergyCapacity { get; set; }

        /// <summary>
        /// Energy request tells the energy storage to send some energy to given component
        /// </summary>
        /// <param name="requestingComponent"> Component that will receive energy</param>
        /// <param name="energyAmount"> The amount of an energy</param>
        void EnergyRequest(IBodyComponent requestingComponent, float energyAmount);

        /// <summary>
        /// Stores given amount of energy
        /// </summary>
        /// <param name="senderComponenet"> Component sending the energy</param>
        /// <param name="energyAmount"> The amount of an energy</param>
        void StoreEnergy(IBodyComponent senderComponenet, float energyAmount);
    }
}
