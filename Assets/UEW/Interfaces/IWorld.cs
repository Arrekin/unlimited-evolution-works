﻿using UEW.World.Date;

namespace UEW.Interfaces
{
    public interface IWorld
    {
        /// <summary>
        /// Current world date in selected world time format
        /// </summary>
        WorldDate CurrentDate { get; }
    }
}
