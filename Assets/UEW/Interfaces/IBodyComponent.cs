﻿using UEW.Bodies;
using UEW.Utils;
using UEW.World;

namespace UEW.Interfaces
{
    public interface IBodyComponent
    {
        /// <summary>
        /// Reference to this component main container
        /// </summary>
        Body Body { get; }

        /// <summary>
        /// Returns how much energy component need to stay alive
        /// </summary>
        float BaseEnergyConsumption { get; }

        /// <summary>
        /// Component Health indicator; Level can have different impact for different components
        /// This value should reflect component condition
        /// </summary>
        float HealthPoints { get; }

        /// <summary>
        /// Returns body componenets overall energy cosumption at the moment(BaseEnergyConsumption+improvements consumptions)
        /// </summary>
        float EnergyConsumption();

        /// <summary>
        /// Grant energy ticket to the component
        /// </summary>
        /// <param name="energyTicket"> Ticket that containes energy for the component</param>
        void ReceiveEnergyTicket(EnergyTicket energyTicket);

        /// <summary>
        /// When this method is called component should request energy ticket from energy storage
        /// Its purpose is to keep component alive as it may need some energy to work even if stay inactive
        /// </summary>
        void Feed();
    }
}
