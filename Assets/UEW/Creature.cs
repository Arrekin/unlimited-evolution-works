﻿using System;
using System.Threading;
using UEW.Exceptions;
using UEW.World;

namespace UEW
{
    public abstract class Creature
    {
        /// <summary>
        /// Indicate whether Creature is currently alive or not
        /// </summary>
        public bool Alive { get; private set; }


        /// <summary>
        /// Avatar class that connects creature with its body in the world
        /// </summary>
        protected Avatar Avatar { get; private set; }

        /// <summary>
        /// Existence Thread that executing Processing methond until creature is alive
        /// </summary>
        private readonly Thread existenceThread;

        protected Creature()
        {
            Alive = false;
            existenceThread = new Thread(Existence);
        }

        /// <summary>
        /// Bringing Creature to the life
        /// </summary>
        public void Animate()
        {
            if( Alive )
                throw new AlreadyAliveException("Trying to ANIMATE already alive creature!");
            if( Avatar == null )
                throw new NullReferenceException("Trying to ANIMATE without assigned AVATAR!");
            Alive = true;
            existenceThread.Start();
        }

        /// <summary>
        /// Kill the creature
        /// </summary>
        public void Kill()
        {
            if (!Alive)
                throw new AlreadyDeadException("Trying to KILL alredy dead creature!");
            Alive = false;
        }

        /// <summary>
        /// All Creature processing code should be put in this method
        /// </summary>
        protected abstract void Processing();

        /// <summary>
        /// Loop for processing data, works in different thread
        /// </summary>
        private void Existence()
        {
            while (Alive)
            {
                Processing();
            }
        }
    }
}
